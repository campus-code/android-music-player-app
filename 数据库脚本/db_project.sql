/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 5.5.53 : Database - db_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_project` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `db_project`;

/*Table structure for table `t_dictionary_info` */

DROP TABLE IF EXISTS `t_dictionary_info`;

CREATE TABLE `t_dictionary_info` (
  `dictionary_id` varchar(32) NOT NULL,
  `code` varchar(50) DEFAULT NULL COMMENT '字典编号',
  `title` varchar(255) DEFAULT NULL COMMENT '字典标题',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '所属上级',
  `classify` varchar(32) DEFAULT NULL COMMENT '字典分类',
  `level` int(1) DEFAULT NULL COMMENT '字典等级',
  `status` int(1) DEFAULT NULL COMMENT '状态：0、禁用，1、启用',
  `create_time` datetime DEFAULT NULL,
  `create_date` int(8) DEFAULT NULL,
  PRIMARY KEY (`dictionary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典表';

/*Data for the table `t_dictionary_info` */

insert  into `t_dictionary_info`(`dictionary_id`,`code`,`title`,`parent_id`,`classify`,`level`,`status`,`create_time`,`create_date`) values 
('0eb7e6ee9dba0662f385c86bbca70a9a','','萝莉','','classify',1,1,'2023-07-13 16:27:56',20230713),
('1104b96b9aa28002c4b7a63e598b7c64','','情感','','classify',1,1,'2023-07-13 16:27:22',20230713),
('19e423f989f6d61b0b68b70dbe7f5860','','史诗','','classify',1,1,'2023-07-13 16:27:04',20230713),
('4bf702cd8370448bb001ec1dac73244e','','浪漫','','classify',1,1,'2023-07-13 16:27:40',20230713),
('5c31516c5d55aff94a723ccfdae91da6','A001','DJ','','classify',1,0,'2023-07-16 10:31:12',20230716),
('ad8d6db284ff235ea8b57704f5123cae','','温馨','','classify',1,1,'2023-07-13 16:27:48',20230713),
('b24faec9ec18445c773e328629898ca6','','震撼','','classify',1,1,'2023-07-13 16:27:11',20230713),
('f596fdd2aa751309a446b072c4712c4a','','励志','','classify',1,1,'2023-07-13 16:26:44',20230713),
('f64cc55165e901951bfe613f7dbf26f1','','正能量','','classify',1,1,'2023-07-13 16:26:54',20230713);

/*Table structure for table `t_music_collection` */

DROP TABLE IF EXISTS `t_music_collection`;

CREATE TABLE `t_music_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_user_id` varchar(32) DEFAULT NULL,
  `music_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

/*Data for the table `t_music_collection` */

insert  into `t_music_collection`(`id`,`sys_user_id`,`music_id`) values 
(11,'6ed8c79e642d71e22c2553cc3c8d8ae0','fed9912cae13f69fee7b51a408a57b2b'),
(12,'57df38831f17197a360d7bc595bb6e99','a48d79473e71875f252c5b9086f1a434'),
(13,'57df38831f17197a360d7bc595bb6e99','744d00a19f53d7979916b66b572da3e9'),
(14,'57df38831f17197a360d7bc595bb6e99','c7c7fd42530f1eae076c993c5670d252'),
(16,'57df38831f17197a360d7bc595bb6e99','4f852706867d9c06f95e8dfdfb61f0ed'),
(17,'6ed8c79e642d71e22c2553cc3c8d8ae0','c7c7fd42530f1eae076c993c5670d252'),
(18,'6ed8c79e642d71e22c2553cc3c8d8ae0','744d00a19f53d7979916b66b572da3e9'),
(22,'6ed8c79e642d71e22c2553cc3c8d8ae0','a48d79473e71875f252c5b9086f1a434'),
(29,'6ed8c79e642d71e22c2553cc3c8d8ae0','4f852706867d9c06f95e8dfdfb61f0ed');

/*Table structure for table `t_music_info` */

DROP TABLE IF EXISTS `t_music_info`;

CREATE TABLE `t_music_info` (
  `music_id` varchar(32) NOT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面',
  `singer` varchar(255) DEFAULT NULL COMMENT '歌手',
  `album` varchar(255) DEFAULT NULL COMMENT '专辑名',
  `lyric` longtext,
  `path` varchar(255) DEFAULT NULL,
  `classify_id` varchar(32) DEFAULT NULL,
  `issue_time` date DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`music_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `t_music_info` */

insert  into `t_music_info`(`music_id`,`title`,`cover`,`singer`,`album`,`lyric`,`path`,`classify_id`,`issue_time`,`create_time`) values 
('4f852706867d9c06f95e8dfdfb61f0ed','燕无歇','1614594052058.jpg','蒋雪儿','燕无歇','[00:00.58]只叹她 回眸秋水被隐去\n[00:06.05]只忆她 点破 去日苦多\n[00:11.51]借三两苦酒 方知离不可\n[00:17.12]只叹她 将思念摇落\n[00:23.96]心多憔悴 爱付与东流的水\n[00:28.95]舍命奉陪 抵不过天公不作美\n[00:34.07]往事回味 不过是弹指一挥\n[00:39.03]日复日望穿秋水恕我愚昧\n[00:44.28]你爱着谁 心徒留几道伤\n[00:49.12]我锁着眉 最是相思断人肠\n[00:54.20]劳燕分飞 寂寥的夜里泪两行\n[00:59.50]烛短遗憾长故人自难忘\n[01:04.35]你爱着谁 心徒留几道伤\n[01:09.35]爱多可悲 恨彼此天涯各一方\n[01:14.40]冷月空对 满腹愁无处话凄凉\n[01:19.67]我爱不悔可孤影难成双\n[01:44.69]心多憔悴 爱付与东流的水\n[01:49.80]舍命奉陪 抵不过天公不作美\n[01:54.78]往事回味 不过是弹指一挥\n[01:59.84]日复日望穿秋水恕我愚昧\n[02:05.15]你爱着谁 心徒留几道伤\n[02:09.96]我锁着眉 最是相思断人肠\n[02:14.98]劳燕分飞 寂寥的夜里泪两行\n[02:20.51]烛短遗憾长故人自难忘\n[02:25.13]你爱着谁 心徒留几道伤\n[02:30.10]爱多可悲 恨彼此天涯各一方\n[02:35.17]冷月空对 满腹愁无处话凄凉\n[02:40.53]我爱不悔可孤影难成双\n[02:45.47]你爱着谁 心徒留几道伤\n[02:50.36]我锁着眉 最是相思断人肠\n[02:55.37]劳燕分飞 寂寥的夜里泪两行\n[03:00.94]烛短遗憾长故人自难忘[ti:燕无歇]\n[ar:蒋雪儿]\n[al:燕无歇]\n[by:]\n[offset:0]\n[00:00.00]燕无歇 - 蒋雪儿\n[00:00.03]词 Lyrics：堇临/刘涛\n[00:00.07]曲 Music：刘涛\n[00:00.09]制作人 Produced by：刘涛\n[00:00.13]编曲 Arranger：谭侃侃\n[00:00.16]男声 Male voice：半阳\n[00:00.20]吉他 Guitar：谭侃侃\n[00:00.23]键盘 Keyboards：谭侃侃\n[00:00.26]合声 Backing vocals： 金天\n[00:00.30]录音棚 Recording studio：北京好乐无荒录音棚\n[00:00.37]录音师 Recording Engineer：侯春阳\n[00:00.41]混音师 Mixing Engineer：侯春阳\n[00:00.45]母带后期混音师 Mastering Engineer：侯春阳\n[00:00.52]监制 Executive producer：陶诗\n[00:00.55]制作公司 Manufacturing company：好乐无荒\n[00:00.58]OP：好乐无荒\n[00:00.58]SP：青风音乐Cheerful Music\n[00:00.58]【未经授权不得翻唱或使用】\n[00:00.58]只叹她 回眸秋水被隐去\n[00:06.05]只忆她 点破 去日苦多\n[00:11.51]借三两苦酒 方知离不可\n[00:17.12]只叹她 将思念摇落\n[00:23.96]心多憔悴 爱付与东流的水\n[00:28.95]舍命奉陪 抵不过天公不作美\n[00:34.07]往事回味 不过是弹指一挥\n[00:39.03]日复日望穿秋水恕我愚昧\n[00:44.28]你爱着谁 心徒留几道伤\n[00:49.12]我锁着眉 最是相思断人肠\n[00:54.20]劳燕分飞 寂寥的夜里泪两行\n[00:59.50]烛短遗憾长故人自难忘\n[01:04.35]你爱着谁 心徒留几道伤\n[01:09.35]爱多可悲 恨彼此天涯各一方\n[01:14.40]冷月空对 满腹愁无处话凄凉\n[01:19.67]我爱不悔可孤影难成双\n[01:44.69]心多憔悴 爱付与东流的水\n[01:49.80]舍命奉陪 抵不过天公不作美\n[01:54.78]往事回味 不过是弹指一挥\n[01:59.84]日复日望穿秋水恕我愚昧\n[02:05.15]你爱着谁 心徒留几道伤\n[02:09.96]我锁着眉 最是相思断人肠\n[02:14.98]劳燕分飞 寂寥的夜里泪两行\n[02:20.51]烛短遗憾长故人自难忘\n[02:25.13]你爱着谁 心徒留几道伤\n[02:30.10]爱多可悲 恨彼此天涯各一方\n[02:35.17]冷月空对 满腹愁无处话凄凉\n[02:40.53]我爱不悔可孤影难成双\n[02:45.47]你爱着谁 心徒留几道伤\n[02:50.36]我锁着眉 最是相思断人肠\n[02:55.37]劳燕分飞 寂寥的夜里泪两行\n[03:00.94]烛短遗憾长故人自难忘','1614594052061.mp3','0eb7e6ee9dba0662f385c86bbca70a9a','2023-07-13','2023-07-13 18:20:52'),
('744d00a19f53d7979916b66b572da3e9','世界第一等','1614619102576.jpg','刘德华','天下第一等','[00:02.62]世界第一等\r\n[00:04.69]作词：李安修、陈富荣 作曲：伍佰\r\n[00:06.45]演唱：刘德华\r\n[00:10.28]\r\n[00:49.63]人生的风景 就像大海的风涌\r\n[00:55.19]有时猛有时平 亲爱朋友你着小心\r\n[01:01.75]人生的环境 乞食嘛会出头天\r\n[01:06.96]莫怨天莫尤人 命顺命歹拢是一生\r\n[01:12.24]\r\n[01:14.42]一杯酒两角眼 三不五时嘛来凑阵\r\n[01:20.27]若要讲博感情 我是世界第一等\r\n[01:26.10]是缘份是注定 好汉剖腹来叁见\r\n[01:31.99]呒惊风呒惊涌 有情有义好兄弟\r\n[01:38.49]\r\n[01:40.25]短短仔的光阴 迫逍着少年时\r\n[01:45.50]求名利无了时 千金难买好人生\r\n[01:52.01]\r\n[02:04.85]人生的风景 亲像大海的风涌\r\n[02:10.51]有时猛有时平 亲爱朋友你着小心\r\n[02:17.12]人生的环境 乞食嘛会出头天\r\n[02:22.82]莫怨天莫尤人 命顺命歹拢是一生\r\n[02:28.64]\r\n[02:53.37]一杯酒两角眼 三不五时嘛来凑阵\r\n[02:59.29]若要讲博感情 我是世界第一等\r\n[03:05.15]是缘份是注定 好汉剖腹来叁见\r\n[03:11.10]呒惊风呒惊涌 有情有义好兄弟\r\n[03:17.06]一杯酒两角眼 三不五时嘛来凑阵\r\n[03:22.86]若要讲博感情 我是世界第一等\r\n[03:28.80]是缘份是注定 好汉剖腹来叁见\r\n[03:34.79]呒惊风呒惊涌 有情有义好兄弟\r\n[03:41.45]\r\n[03:42.93]短短仔的光阴 迫逍着少年时\r\n[03:48.08]求名利无了时 千金难买好人生\r\n[03:54.62]','1614619755263.mp3','19e423f989f6d61b0b68b70dbe7f5860','2023-07-13','2023-07-13 01:18:23'),
('a48d79473e71875f252c5b9086f1a434','窗外','1689219941384.jpg','李琛','窗外','[00:02.00]窗外\r\n[00:06.69]作词 : 牛朝阳 作曲 : 牛朝阳\r\n[00:15.45]演唱：李琛\r\n[00:29.50]今夜我又来到你的窗外\r\n[00:33.50]窗帘上你的影子多么可爱\r\n[00:38.50]悄悄的爱过你这么多年\r\n[00:43.20]明天我就要离开\r\n[00:46.50]多少回我来到你的窗外\r\n[00:52.00]也曾想敲敲门叫你出来\r\n[00:56.00]想一想你的美丽 我的平凡\r\n[01:01.00]一次次默默走开\r\n[01:05.00]再见了心爱的梦中女孩\r\n[01:10.00]我将要去远方寻找未来\r\n[01:15.00]假如我有一天荣归故里\r\n[01:19.10]再到你窗外诉说情怀\r\n[01:24.10]再见了心爱的梦中女孩\r\n[01:28.10]对着你的影子说声珍重\r\n[01:32.40]假如我永远不再回来\r\n[01:37.40]就让月亮守在你窗外\r\n[01:42.42]\r\n[02:01.10]今夜我又来到你的窗外\r\n[02:06.20]窗帘上你的影子多么可爱\r\n[02:11.50]悄悄的爱过你这么多年\r\n明天我就要离开\r\n多少回我来到你的窗外\r\n也曾想敲敲门叫你出来\r\n想一想你的美丽\r\n我的平凡\r\n一次次默默走开\r\n再见了心爱的梦中女孩\r\n我将要去远方寻找未来\r\n假如我有一天荣归故里\r\n再到你窗外诉说情怀\r\n再见了心爱的梦中女孩\r\n对着你的影子说声珍重\r\n假如我永远不再回来\r\n就让月亮守在你窗外\r\n再见了心爱的梦中女孩\r\n我将要去远方寻找未来\r\n假如我有一天荣归故里\r\n再到你窗外诉说情怀\r\n再见了心爱的梦中女孩\r\n对着你的影子说声珍重\r\n假如我永远不再回来\r\n就让月亮守在你窗外\r\n就让月亮守在你窗外','1689219941391.mp3','4bf702cd8370448bb001ec1dac73244e','2023-07-13','2023-07-13 11:45:41'),
('c7c7fd42530f1eae076c993c5670d252','外婆','1614619005382.jpg','周杰伦','肖邦','[00:00.20]~外婆~\n[00:03.10]歌: 周杰伦\n[00:06.00]作词: 周杰伦 作曲: 周杰伦\n[00:08.90]合声编写: 周杰伦\n[00:11.80]合声: 周杰伦 张欣瑜 女声: 张欣瑜\n[00:14.70]编曲: 周杰伦\n[00:17.60]制作：MP3.5nd.com\n[00:20.50]\n[00:23.63]今天是外婆生日\n[00:24.88]我换上复古西装\n[00:26.40]载着外婆开着拉风的古董车兜兜兜风\n[00:29.61]车里放着她的最爱\n[00:30.91]找回属于是她的时代\n[00:32.44]往大稻埋码头开去\n[00:33.84]把所有和外公的往事静静回忆\n[00:36.14]外婆她脸上的涟漪\n[00:37.41]美丽但藏不住压抑\n[00:39.04]失去了爱情只盼望亲情\n[00:41.42]弥补回应\n[00:42.02]大人们以为出门之前\n[00:43.39]桌上放六百就算是孝敬\n[00:44.79]一天到晚拼了命\n[00:46.28]赚钱少了关怀有什么意义\n[00:47.65]\n[00:48.15]外婆她的期待\n[00:50.82]慢慢变成无奈\n[00:53.77]大人们始终不明白\n[00:59.96]她要的是陪伴\n[01:02.86]而不是六百块\n[01:05.84]比你给的还简单\n[01:09.67]\n[01:11.72]外婆她的无奈\n[01:14.49]无法变成期待\n[01:17.35]只有爱才能够明白\n[01:23.36]走在淡水河衅\n[01:26.25]听着她的最爱\n[01:29.30]把温暖放回口袋\n[01:33.65]\n[01:34.77]记得去年外婆的生日\n[01:36.21]表哥带我和外婆参加\n[01:37.76]她最最重视的颁奖典礼\n[01:40.27]结果却拿不到半个奖\n[01:42.30]不知该笑不笑\n[01:43.67]我对着镜头傻笑\n[01:45.22]只觉得自己可笑\n[01:46.62]我难过\n[01:47.37]却不是因为没有得奖而难过\n[01:49.57]我失落\n[01:50.26]是因为看到外婆失落而失落\n[01:52.50]大人们根本不能体会\n[01:54.05]表哥他的用心\n[01:55.45]好像随他们高兴就可以彻底的否定\n[01:58.48]否定我的作品\n[01:59.90]决定在于心情\n[02:01.30]想坚持风格他们就觉得还欧颗\n[02:04.04]没惊喜没有改变\n[02:05.34]我已经听了三年\n[02:06.81]我告诉外婆 我没输 不需要改变\n[02:09.82]表哥说不要觉得可惜\n[02:11.69]这只是一场游戏\n[02:13.15]只要外婆觉得好听\n[02:14.61]那才是一种鼓励\n[02:16.17]外婆露出了笑容 说她以我为荣\n[02:18.54]浅浅的笑容\n[02:19.71]就让我感到比得奖它还要光荣\n[02:21.73]\n[02:22.64]外婆她的期待\n[02:25.28]慢慢变成无奈\n[02:28.30]大人们始终不明白\n[02:34.21]她要的是陪伴\n[02:37.22]而不是六百块\n[02:40.24]比你给的还简单\n[02:44.23]\n[02:45.62]外婆她的无奈\n[02:49.02]无法期待\n[02:51.66]只有爱才能够明白\n[02:57.68]走在淡水河衅\n[03:00.64]听着她的最爱\n[03:03.56]把温暖放回口袋\n[03:08.85]\n[03:09.94]外婆她的期待\n[03:12.65]慢慢变成无奈\n[03:15.60]大人们始终不明白\n[03:21.64]她要的是陪伴\n[03:24.49]而不是六百块\n[03:27.48]比你给的还简单\n[03:32.17]\n[03:33.48]外婆她的无奈\n[03:36.31]无法变成期待\n[03:39.32]只有爱才能够明白\n[03:45.14]走在淡水河衅\n[03:48.07]听着她的最爱\n[03:51.05]把温暖放回口袋\n[03:55.59]','1614620161403.mp3','0eb7e6ee9dba0662f385c86bbca70a9a','2023-07-13','2023-07-13 01:16:45'),
('fe45fea2b3b6966725f29b126f3bd76e','无愧于心1','1689478255905.jpg','孙楠','无愧于心','','1689478281250.mp3','f596fdd2aa751309a446b072c4712c4a','2023-07-01','2023-07-16 11:32:13'),
('fed9912cae13f69fee7b51a408a57b2b','桥边姑娘','1614620054520.jpg','海伦','首张','[00:00.00]《桥边姑娘》\n[00:04.00]词/曲/唱：海伦\n[00:07.00]\n[00:09.00]歌词编辑：孟德良\n[00:12.00]2020年02月25日\n[00:15.00]\n[00:16.79]暖阳下 我迎芬芳\n[00:19.80]是谁家的姑娘\n[00:22.77]我走在了那座 小桥上\n[00:26.09]你抚琴奏忧伤\n[00:28.38]\n[00:29.08]桥边歌唱的 小姑娘\n[00:32.19]你眼角在流淌\n[00:35.29]你说一个人 在逞强\n[00:38.61]一个人念家乡\n[00:40.89]\n[00:41.59]风华模样 你落落大方\n[00:47.87]坐在桥上 我听你歌唱\n[00:53.51]\n[00:56.41]我说桥边姑娘 你的芬芳\n[01:03.34]我把你放心上\n[01:06.37]刻在了我心膛\n[01:08.54]\n[01:09.54]桥边姑娘 你的忧伤\n[01:15.78]我把你放心房\n[01:18.83]不想让你流浪\n[01:23.85](Music)\n[01:37.63]暖阳下的桥头旁\n[01:40.53]有这样一姑娘\n[01:43.52]她有着长长的 乌黑发\n[01:46.75]一双眼明亮\n[01:49.04]\n[01:49.74]姑娘你让我心荡漾\n[01:52.93]小鹿在乱撞\n[01:56.03]你说无人在身旁\n[01:59.15]一个人在流浪\n[02:01.53]\n[02:02.23]风华模样 你落落大方\n[02:08.46]坐在桥上 我听你歌唱\n[02:14.33]\n[02:17.23]我说桥边姑娘 你的芬芳\n[02:23.95]我把你放心上\n[02:27.14]刻在了我心膛\n[02:29.51]\n[02:30.21]桥边姑娘 你的忧伤\n[02:36.44]我把你放心房\n[02:39.56]不想让你流浪','1614620054523.mp3','0eb7e6ee9dba0662f385c86bbca70a9a','2023-07-13','2023-07-13 01:16:24');

/*Table structure for table `t_sys_resources` */

DROP TABLE IF EXISTS `t_sys_resources`;

CREATE TABLE `t_sys_resources` (
  `resource_id` varchar(32) NOT NULL,
  `title` varchar(10) DEFAULT NULL COMMENT '标题',
  `url` varchar(50) DEFAULT NULL COMMENT '请求地址',
  `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `visible` bit(1) DEFAULT NULL COMMENT '是否可见',
  `style` varchar(255) DEFAULT NULL,
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父类编号',
  `type` int(1) DEFAULT NULL COMMENT '按钮类型，0、工具栏，1、操作条',
  `classify` varchar(255) DEFAULT NULL COMMENT '资源分类，目录、菜单、按钮',
  `sort` int(4) DEFAULT NULL COMMENT '排序',
  `ascription` int(2) DEFAULT NULL COMMENT '资源归属，1、后台，2、app，3、其他',
  `grouping` varchar(255) DEFAULT NULL COMMENT '目录分组',
  `status` int(1) DEFAULT NULL COMMENT '状态，0、禁用，1、启用，-1、删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`resource_id`) USING BTREE,
  KEY `parent_id` (`parent_id`),
  KEY `classify` (`classify`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统资源表';

/*Data for the table `t_sys_resources` */

insert  into `t_sys_resources`(`resource_id`,`title`,`url`,`perms`,`icon`,`visible`,`style`,`parent_id`,`type`,`classify`,`sort`,`ascription`,`grouping`,`status`,`create_time`,`last_update_time`) values 
('0319092f56fa4f394790b9cae5c5c237','删除','delete','','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','bb55699087e3af9f17b89e9a178bb7bb',NULL,'BUTTON',3,NULL,'',1,'2023-07-13 18:43:49','2023-07-13 18:43:49'),
('059358cc4487e7773a878cda0193de62','修改','edit','settings:resource:modify','&#xe642;','','layui-btn layui-btn-sm layui-btn-normal','3992d019fa88267eebcba4948e3ef6c1',0,'BUTTON',2,1,NULL,1,'2023-07-13 17:03:36','2023-07-13 22:01:11'),
('0876f67d9d0ce57c274b72d67b0b8644','还原','reduction','dustbin:tmall:new:reduction','','','layui-btn layui-btn-sm','8a9f73fa4aeb410284cf12925f31fa63',0,'BUTTON',1,1,NULL,1,'2023-07-13 17:52:05','2023-07-13 22:01:11'),
('0e2a1968f8a5d2a29bce111d4d1ca69b','删除角色','delete','sys:role:delete','','','layui-btn layui-btn-sm layui-btn-danger','b8915891e54932df280785df69ab1b5a',0,'BUTTON',2,1,NULL,1,'2023-07-13 16:01:55','2023-07-13 22:01:11'),
('1389cc79b92ceddfe21b6fbed368a531','修改','edit','','&#xe642;','','layui-btn layui-btn-sm layui-btn-normal','bb55699087e3af9f17b89e9a178bb7bb',NULL,'BUTTON',2,NULL,'',1,'2023-07-13 18:43:33','2023-07-13 18:56:44'),
('15ff00cd8cfb9f0334402b3792cc2744','修改','edit','resources:dictionary:modify','&#xe642;','','layui-btn layui-btn-sm layui-btn-normal','e14872a2d9c5865429eab89a512fbd44',NULL,'BUTTON',2,NULL,'',1,'2023-07-13 20:13:02','2023-07-13 20:13:02'),
('2134076d72b4b7d88b4a878c4065b60f','删除','delete','settings:resource:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','3992d019fa88267eebcba4948e3ef6c1',NULL,'BUTTON',3,1,NULL,1,'2023-07-13 03:19:33','2023-07-13 22:01:11'),
('35d48b39edbe08e6a657674a8e2e901f','删除','delete','settings:role:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','dea9872e3c1a186f2ff9bbfb63351a6f',0,'BUTTON',3,1,NULL,1,'2023-07-13 18:20:48','2023-07-13 22:01:11'),
('3992d019fa88267eebcba4948e3ef6c1','资源管理','settings/resource/list','settings:resource:list','','',NULL,'53bd3a90db3a261a76e45c61cfc489d4',NULL,'MENU',1,1,NULL,1,'2023-07-13 15:17:22','2023-07-13 22:01:11'),
('46969de817040b955c8fb3911b8691b6','添加角色','add','sys:role:add','&#xe654;','','layui-btn layui-btn-sm','b8915891e54932df280785df69ab1b5a',0,'BUTTON',1,1,NULL,1,'2023-07-13 15:51:13','2023-07-13 22:01:11'),
('4903fa7d0ce151223015f4e3ee5890bf','删除','delete','resources:staff:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','80750adc8251f609797cc532c3aa5421',0,'BUTTON',3,1,'',1,'2023-07-13 09:30:11','2023-07-13 00:07:08'),
('51e36593a943a0f25bcadbba630f376f','还原','reduction','dustbin:tmall:abroad:reduction','','','layui-btn layui-btn-sm','22ea663c6f25cb1c70317c0e11affb9a',0,'BUTTON',1,1,NULL,1,'2023-07-13 01:09:10','2023-07-13 22:01:11'),
('53bd3a90db3a261a76e45c61cfc489d4','开发配置','','','','',NULL,'',NULL,'CATALOG',1,NULL,'authority',1,NULL,'2023-07-13 22:01:11'),
('58c33112974e8a4ccb7b77d01e0e6375','删除','delete','resources:dictionary:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','e14872a2d9c5865429eab89a512fbd44',NULL,'BUTTON',3,NULL,'',1,'2023-07-13 20:13:30','2023-07-13 20:13:30'),
('643986b77c9a659f9f918e2de045aab0','还原','reduction','dustbin:tmall:old:reduction','','','layui-btn layui-btn-sm','a7387bff5209ba22bd8ff6af79bec3a1',0,'BUTTON',1,1,NULL,1,'2023-07-13 01:08:42','2023-07-13 22:01:11'),
('74ab62cfb0c0e1ab742fa8ec50e4ea68','查看','view','dustbin:tmall:old:view','&#xe705;','','layui-btn layui-btn-sm layui-btn-warm','a7387bff5209ba22bd8ff6af79bec3a1',0,'BUTTON',2,1,NULL,1,'2023-07-13 01:11:45','2023-07-13 22:01:11'),
('7cac58f5e912a81c61e55ffbe92e36c1','删除','delete','dustbin:tmall:old:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','a7387bff5209ba22bd8ff6af79bec3a1',0,'BUTTON',4,1,NULL,1,'2023-07-13 01:47:27','2023-07-13 22:01:11'),
('80750adc8251f609797cc532c3aa5421','用户管理','resources/staff/list','resources:staff:list','','','','98803ee7e10ccb7a06400e33b37ec7df',NULL,'MENU',2,1,'',1,'2023-07-13 02:05:22','2023-07-13 13:14:16'),
('98803ee7e10ccb7a06400e33b37ec7df','资源管理','','','','','','',NULL,'CATALOG',2,NULL,'user',1,NULL,'2023-07-13 17:24:11'),
('a4451d6e3cbfe5051f8f7b2e24caa2df','查看','view','dustbin:tmall:abroad:view','&#xe705;','','layui-btn layui-btn-sm layui-btn-warm','22ea663c6f25cb1c70317c0e11affb9a',0,'BUTTON',2,1,NULL,1,'2023-07-13 01:12:20','2023-07-13 22:01:11'),
('a5b91e0416600298e32fa9ea4f39639a','修改','edit','resources:staff:modify','&#xe642;','','layui-btn layui-btn-sm layui-btn-normal','80750adc8251f609797cc532c3aa5421',0,'BUTTON',2,1,'',1,'2023-07-13 09:29:52','2023-07-13 00:06:58'),
('b31def5ceaf1fab0dd8848c371e5132e','添加','add','resources:staff:save','&#xe654;','','layui-btn layui-btn-sm','80750adc8251f609797cc532c3aa5421',0,'BUTTON',1,1,'',1,'2023-07-13 02:16:38','2023-07-13 00:06:46'),
('b8915891e54932df280785df69ab1b5a','资源权限','authority/resources/setting','sys:authority:page','','',NULL,'ce6d938bde4b9ac904ae971682976936',NULL,'MENU',1,1,NULL,1,'2023-07-13 03:35:39','2023-07-13 22:01:11'),
('ba9ea0fc67d525f0b8269527cef3eae8','查看','view','dustbin:tmall:new:view','&#xe705;','','layui-btn layui-btn-sm layui-btn-warm','8a9f73fa4aeb410284cf12925f31fa63',0,'BUTTON',2,1,NULL,1,'2023-07-13 16:07:37','2023-07-13 22:01:11'),
('bb55699087e3af9f17b89e9a178bb7bb','音乐管理','resources/music/list','','','','','98803ee7e10ccb7a06400e33b37ec7df',NULL,'MENU',2,NULL,'',1,'2023-07-13 18:42:50','2023-07-13 17:23:53'),
('bcc5ec4b4b74944a31f91db81c1c475e','添加','add','settings:role:save','&#xe654;','','layui-btn layui-btn-sm','dea9872e3c1a186f2ff9bbfb63351a6f',0,'BUTTON',1,1,NULL,1,'2023-07-13 18:19:38','2023-07-13 22:01:11'),
('cdb357370d5b361a008c557f985e44d4','添加','add','','&#xe654;','','layui-btn layui-btn-sm','bb55699087e3af9f17b89e9a178bb7bb',NULL,'BUTTON',1,NULL,'',1,'2023-07-13 18:43:10','2023-07-13 18:43:10'),
('ce6d938bde4b9ac904ae971682976936','权限设置','','','','',NULL,'',NULL,'CATALOG',2,1,'authority',1,'2023-07-13 15:15:36','2023-07-13 22:01:11'),
('ce6d938bde4b9ac904ae971682976937','添加','add','settings:resource:save','&#xe654;','','layui-btn layui-btn-sm','3992d019fa88267eebcba4948e3ef6c1',0,'BUTTON',1,1,NULL,1,'2023-07-13 17:02:59','2023-07-13 22:01:11'),
('cf3e0cb36a32711f56e1a59eae089117','删除','delete','dustbin:tmall:new:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','8a9f73fa4aeb410284cf12925f31fa63',0,'BUTTON',3,1,NULL,1,'2023-07-13 16:07:55','2023-07-13 22:01:11'),
('d0f33023c9a0848e65db5606c336e3ca','添加','add','resources:dictionary:save','&#xe654;','','layui-btn layui-btn-sm','e14872a2d9c5865429eab89a512fbd44',NULL,'BUTTON',1,NULL,'',1,'2023-07-13 20:11:26','2023-07-13 20:11:50'),
('d11ace81698866e3f35ce9e316c28b1d','修改角色','edit','sys:role:edit','','','layui-btn layui-btn-sm layui-btn-normal','b8915891e54932df280785df69ab1b5a',0,'BUTTON',1,1,NULL,1,'2023-07-13 16:01:37','2023-07-13 22:01:11'),
('d23726402e41042149ac9467c8de43e1','保存','save','sys:authority:save','&#xe654;','','layui-btn layui-btn-sm','b8915891e54932df280785df69ab1b5a',NULL,'BUTTON',1,1,NULL,1,'2023-07-13 05:45:55','2023-07-13 22:01:11'),
('d4695b2d99addc0bbc1de95d4d72a822','删除','delete','dustbin:tmall:abroad:delete','&#xe640;','','layui-btn layui-btn-sm layui-btn-danger','22ea663c6f25cb1c70317c0e11affb9a',0,'BUTTON',4,1,NULL,1,'2023-07-13 01:46:53','2023-07-13 22:01:11'),
('d589cb313d9ca471bfc8310a7405cc44','修改','edit','settings:role:modify','&#xe642;','','layui-btn layui-btn-sm layui-btn-normal','dea9872e3c1a186f2ff9bbfb63351a6f',0,'BUTTON',2,1,NULL,1,'2023-07-13 18:20:01','2023-07-13 22:01:11'),
('dea9872e3c1a186f2ff9bbfb63351a6f','角色管理','settings/role/list','settings:role:list','','',NULL,'53bd3a90db3a261a76e45c61cfc489d4',NULL,'MENU',5,NULL,NULL,1,NULL,'2023-07-13 22:01:11'),
('e14872a2d9c5865429eab89a512fbd44','字典管理','resources/dictionary/list','resources:dictionary:list','','','','98803ee7e10ccb7a06400e33b37ec7df',NULL,'MENU',4,NULL,'',1,'2023-07-13 20:10:45','2023-07-13 20:24:11');

/*Table structure for table `t_sys_role_authority` */

DROP TABLE IF EXISTS `t_sys_role_authority`;

CREATE TABLE `t_sys_role_authority` (
  `authority_id` varchar(32) NOT NULL COMMENT '权限号',
  `resource_id` varchar(32) DEFAULT NULL COMMENT '资源号',
  `role_id` varchar(32) DEFAULT NULL COMMENT '角色号',
  PRIMARY KEY (`authority_id`) USING BTREE,
  KEY `index_authority_menu_id` (`resource_id`) USING BTREE,
  KEY `index_authority_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户权限表';

/*Data for the table `t_sys_role_authority` */

insert  into `t_sys_role_authority`(`authority_id`,`resource_id`,`role_id`) values 
('00b7e6243e6ab97e3d33f4f6438a9361','e1aa11fa00603134d4a960177f25b078','25584e184e69b7df3a842cad90e5dc55'),
('00d7206df69560104f2d3bdf76c4e06a','8271911c11770eaf05e8e0705d65a3b4','2591ccc345c99ffefed5e3572c5624b9'),
('0141e488720f4a5d88ae49247fb6a23d','ac5d77d0cb87c8cbe2cf7881a5ae75e7','25584e184e69b7df3a842cad90e5dc55'),
('01c50d738db8245c788ac735401cdc91','be5299c14a42a0fba0ca46ed13183853','2591ccc345c99ffefed5e3572c5624b9'),
('0307466a643f0c70ba3df73c827eec95','7c476cd3d38c2343e76030447fc142c4','132df2301625dc37fd1b4c258d4a2838'),
('037cd17cde403a3c9456f290ae68986c','b3c777ecec0a944227b7d8f8261b51d1','132df2301625dc37fd1b4c258d4a2838'),
('06ecdd7ef5581b784cfe1f5106fecee5','a923938164c7f04451362128770a58c8','2591ccc345c99ffefed5e3572c5624b9'),
('07e06508b3e469f5674a2e000376cbde','0319092f56fa4f394790b9cae5c5c237','dadd65d82a694cc49288c930fd5527f5'),
('084bfff9796430a04e4433491cf5a6f0','48a630e30197b7a6752dff24c4eefdf7','132df2301625dc37fd1b4c258d4a2838'),
('0c0962d8cb8eb4e41c10b3ec7fb26075','b8915891e54932df280785df69ab1b5a','2591ccc345c99ffefed5e3572c5624b9'),
('0f6bbe8374f6cff66a0dfaf6b4880b10','46969de817040b955c8fb3911b8691b6','dadd65d82a694cc49288c930fd5527f5'),
('1133f42f069028f7e226981bba5154ef','44ec659dcc308895951b4521e13469c4','2591ccc345c99ffefed5e3572c5624b9'),
('12abe5595ad5f9dbb51997df8a5ec91b','d23726402e41042149ac9467c8de43e1','2591ccc345c99ffefed5e3572c5624b9'),
('12ff020b7738c4026a42fd9dcf63eeb8','8c23b7714200c43cfeef247b9e745058','132df2301625dc37fd1b4c258d4a2838'),
('1334a2cc22ff949cfc37d45ea8ea6135','0876f67d9d0ce57c274b72d67b0b8644','132df2301625dc37fd1b4c258d4a2838'),
('1576db0cd86296615d18560940b159bf','0e2a1968f8a5d2a29bce111d4d1ca69b','2591ccc345c99ffefed5e3572c5624b9'),
('15b4aa920ca19ab35cbee6d39d0856f8','f06d2505422ae5eeb2d4383bce1ec308','25584e184e69b7df3a842cad90e5dc55'),
('1693fc3ed1557f7567fd01b915ba6e49','7273b375695b6301e362ac260b7101ef','2591ccc345c99ffefed5e3572c5624b9'),
('1752d94de0df607ddcad2a4dfe954fb5','63931ada9418b9a0d6e16c5e9240a60b','2591ccc345c99ffefed5e3572c5624b9'),
('187232c272a9fdcdf5bbe20b4ba6c8d3','b31def5ceaf1fab0dd8848c371e5132e','2591ccc345c99ffefed5e3572c5624b9'),
('1928771339c4a31fcb4575850facdc36','530963cbd5f0cf47ec3e150b70582506','2591ccc345c99ffefed5e3572c5624b9'),
('1abc56a93470f0cd54c39bbe41196ec6','48a630e30197b7a6752dff24c4eefdf7','2591ccc345c99ffefed5e3572c5624b9'),
('1b63441f87757a4c288536986dc5612d','7cac58f5e912a81c61e55ffbe92e36c1','2591ccc345c99ffefed5e3572c5624b9'),
('1c1e4cff74d624f4a24124466e18ca8a','687cfc35696d00f16192846082ed562b','132df2301625dc37fd1b4c258d4a2838'),
('1c5ea7c59546c758a85466304acb478c','4252497f4fceeaebef3ec62a29e21d80','132df2301625dc37fd1b4c258d4a2838'),
('1f403568b4c02d64ba02210bbf940097','72acc23340c2fbe1088afefed6ba428a','2591ccc345c99ffefed5e3572c5624b9'),
('1f6b4bb0b8d6bb5a90d66c472de8c3d0','8a9f73fa4aeb410284cf12925f31fa63','b5e6af275bd0cfacc09bf9af7550e3c7'),
('1f7e46ba007617947c156dedb4c09ae2','0c897c7a830230131fdf1294952d268c','132df2301625dc37fd1b4c258d4a2838'),
('20abfd6fb33dc8f40c604f29a18a43f6','35ba3a88e51b9c0a0da89afafaa04e28','2591ccc345c99ffefed5e3572c5624b9'),
('20bf428f05f50b0ac6a488f9141d4668','a535d1b325bc33b6b69890a2fc0df471','132df2301625dc37fd1b4c258d4a2838'),
('244ce80e979910e936b8a22570ee5911','a5b91e0416600298e32fa9ea4f39639a','2591ccc345c99ffefed5e3572c5624b9'),
('245c3c0287d65df27a72e198a6a8875f','4d0fff6c34b9905f84fdecd114009377','2591ccc345c99ffefed5e3572c5624b9'),
('24ed50ad6ea24a9d2c75d37d56d9f32a','b6027c98b83fad392b037d73ed8b5573','132df2301625dc37fd1b4c258d4a2838'),
('255bf43b84b2f12e03279dc0ea9cac26','2697a012c49c10074ad0c6483266bdf0','2591ccc345c99ffefed5e3572c5624b9'),
('25dcaebd73f25fcfb8917f33b57e75d1','4db7ce6ad6a41bda81b0bea914001826','132df2301625dc37fd1b4c258d4a2838'),
('26cdb71b96d4f7bf2b538eefaf6a6ef1','7005e4d4f496295b5faf2daca852eaca','2591ccc345c99ffefed5e3572c5624b9'),
('270241526615a50c856401f2a6f4f18b','8a2429c86f2258a88d0b94354cab6c0f','2591ccc345c99ffefed5e3572c5624b9'),
('27970ceedaf320ec667d5e085969ea10','9aab90cd40e432f02ca74d6274789242','132df2301625dc37fd1b4c258d4a2838'),
('289a196b57ec8a2c1c74b5c9c82d5f20','ba9ea0fc67d525f0b8269527cef3eae8','132df2301625dc37fd1b4c258d4a2838'),
('2ab80fb149768d60c0a2fc0d9ae06a4c','7e3bf80e3941afe908be70ba8b9603f8','2591ccc345c99ffefed5e3572c5624b9'),
('2ad5df57b05a321c67b0cb1cb700c7e2','b4624a3d5da9633edf42f475bd5ed512','2591ccc345c99ffefed5e3572c5624b9'),
('2ca307682c44ca213c7bc64134b97e5d','9aab90cd40e432f02ca74d6274789242','2591ccc345c99ffefed5e3572c5624b9'),
('2ea890ada643b0348559a6f06378d4a1','0f69ab4acb179ec37147607cfebb4246','2591ccc345c99ffefed5e3572c5624b9'),
('2eec0c07339afc2c177175032d32756f','0e2a1968f8a5d2a29bce111d4d1ca69b','dadd65d82a694cc49288c930fd5527f5'),
('2f02e102986a253984af4f5503f178ec','164da843195844500b1b881b574e4420','2591ccc345c99ffefed5e3572c5624b9'),
('2f6be3521174850d5880bd34d6959c5c','7255875725e897036ee1eafc63efa67c','2591ccc345c99ffefed5e3572c5624b9'),
('2f986cdfe5a3b603486a08a27dd23c61','b6027c98b83fad392b037d73ed8b5573','2591ccc345c99ffefed5e3572c5624b9'),
('2fcdead4debbc694b1f780212386baec','643986b77c9a659f9f918e2de045aab0','2591ccc345c99ffefed5e3572c5624b9'),
('303ae5ceca4d6415075c94dc7262b908','4903fa7d0ce151223015f4e3ee5890bf','dadd65d82a694cc49288c930fd5527f5'),
('314159d7df56c95b16b405222463b24e','7c476cd3d38c2343e76030447fc142c4','2591ccc345c99ffefed5e3572c5624b9'),
('325f4a7e6e81106b117d248eb622a2e7','82156e74423c47004a8974f3a6b15760','2591ccc345c99ffefed5e3572c5624b9'),
('343e649b6608efad9c846715027c6896','ce6d938bde4b9ac904ae971682976936','2591ccc345c99ffefed5e3572c5624b9'),
('350ee6e06101d7f4d69332dcf844e465','687cfc35696d00f16192846082ed562b','2591ccc345c99ffefed5e3572c5624b9'),
('362b29820faed0140ecc360418238cfb','d854cbd43bfc338c3b361a1f2761b04f','25584e184e69b7df3a842cad90e5dc55'),
('364a54edb38665d9e9c6a754cd816046','60e9528451e89149de5cd1c2b28215a0','2591ccc345c99ffefed5e3572c5624b9'),
('3724402b642e97c698c9a239898f9fee','53bd3a90db3a261a76e45c61cfc489d4','dadd65d82a694cc49288c930fd5527f6'),
('38ada6fc9c2a8506b6e9227796299b1a','0e8a1aba3c2498b621a06d3656d41615','132df2301625dc37fd1b4c258d4a2838'),
('399d008cc56794f63aec2a78bb8d936c','334c8e3d31d084817ead4e11091ef706','2591ccc345c99ffefed5e3572c5624b9'),
('3a2a43ce10c87db1331b469d24a01f0f','c990fd15c96dc05a17167b0272d52930','132df2301625dc37fd1b4c258d4a2838'),
('3eda854be48b09e45c62e0fbea3917b9','8641d996b53ee08925dddc992d8e9f9d','2591ccc345c99ffefed5e3572c5624b9'),
('3fad5745da91246c19cca650c922c433','b78d9aad0da7760019a851c367de8298','132df2301625dc37fd1b4c258d4a2838'),
('4077e3eb92827858e12d6d961a7277ee','3e9f1b133aa33315203d15c7c9b40ca1','2591ccc345c99ffefed5e3572c5624b9'),
('411fc49e17b9f528859599d715e78207','f470e40184444d69eb466f1be305cd66','2591ccc345c99ffefed5e3572c5624b9'),
('4180f32d27016d47991cfd6f2cead5d1','4c318706940095c0f1a6a680852679e7','132df2301625dc37fd1b4c258d4a2838'),
('41d2db019346ad94fc9938979b2a1d82','0900656adf85cf45b2d2fb3cc6bc7374','2591ccc345c99ffefed5e3572c5624b9'),
('41f76bf1ecb1b4747a1ffe1b94c76f36','d731bd18cc0bc633d7df44e8dc225f01','2591ccc345c99ffefed5e3572c5624b9'),
('43e193f18c3516f5f53627c07d4d3181','b64eef98cd39ba97a43b22cd2155f506','2591ccc345c99ffefed5e3572c5624b9'),
('46a0fe003637d0d8af77c8a5e1bbf689','bb55699087e3af9f17b89e9a178bb7bb','dadd65d82a694cc49288c930fd5527f5'),
('46c813e7cfe2f39faf73a7ae9a741a32','53bd3a90db3a261a76e45c61cfc489d4','dadd65d82a694cc49288c930fd5527f5'),
('479dc01bde6a81de795ffbea9097b5b6','6b1afce9c445e0e96a03f902d2a05ea3','2591ccc345c99ffefed5e3572c5624b9'),
('47e397db2dba9bef0d21962ebb6f20e7','a923938164c7f04451362128770a58c8','132df2301625dc37fd1b4c258d4a2838'),
('48bcc727ef124636fb5bc0c35f03aefa','3992d019fa88267eebcba4948e3ef6c1','dadd65d82a694cc49288c930fd5527f5'),
('48f2ad83e86363569aadc6e8d2460576','3992d019fa88267eebcba4948e3ef6c1','dadd65d82a694cc49288c930fd5527f6'),
('49280f6d59dbd6e9d34a362396df4afa','debf9924137846b52d8d06fd38ad44ac','2591ccc345c99ffefed5e3572c5624b9'),
('49d328aee6f9916735916de8de78e467','9dade9b786511b2e49600997396150c6','2591ccc345c99ffefed5e3572c5624b9'),
('4d3b18ac80a8dac8f58d3e3947d4a0a5','c990fd15c96dc05a17167b0272d52930','2591ccc345c99ffefed5e3572c5624b9'),
('4da167b38d5a196d0ed163a2e45245ae','05332425de2d63066c482b5d3bb1e619','2591ccc345c99ffefed5e3572c5624b9'),
('4dcf581e76828080e87e8002f40f696e','b1c31fb1b64259b8ffb83a31bc445475','2591ccc345c99ffefed5e3572c5624b9'),
('4edb44e520c5280a160c649552b1c4c9','a5e91c5241c2e774c7f70cf57e10e360','2591ccc345c99ffefed5e3572c5624b9'),
('4ef4678aa231e0aa9ec0a13def5ae8f0','2491ae6d4a046a60ab27c9ed33d26a89','132df2301625dc37fd1b4c258d4a2838'),
('4f857077689c549d036591e4714956bf','1ec4c06d2e81521ae43641721bfcddf0','2591ccc345c99ffefed5e3572c5624b9'),
('5021d81882240f6de973b944c0617ab4','be1ac761243b1622c9c6071ed473ac73','2591ccc345c99ffefed5e3572c5624b9'),
('50fbec23a11a89e7a03767d38252e00f','d731bd18cc0bc633d7df44e8dc225f01','25584e184e69b7df3a842cad90e5dc55'),
('52203e7615a69ac5d47fb609c511a647','a939e3f90cae43e648704d87986753d9','2591ccc345c99ffefed5e3572c5624b9'),
('528c32467a4f07b09cda39090c3a7e25','a50db57101fe24ffba13e095dcc773d7','2591ccc345c99ffefed5e3572c5624b9'),
('533055766494eb1db7be4e16a0c9df9b','0e8a1aba3c2498b621a06d3656d41615','2591ccc345c99ffefed5e3572c5624b9'),
('53bc1dfccad60175fa8693633a89fbe0','8e8bee9cbc214fe4e09458c78f211212','25584e184e69b7df3a842cad90e5dc55'),
('53ddb6affbb72ac2e0549c4e7616b5e9','32d1adae2fc0a2411e018d88f2b5ec84','2591ccc345c99ffefed5e3572c5624b9'),
('5401608a84495f27737b5be6f1f21f4d','e1aa11fa00603134d4a960177f25b078','2591ccc345c99ffefed5e3572c5624b9'),
('55251fb80296d9c9eda4fea82467a40a','4383c2fb67066e4c8e3fd2ebba363dbe','2591ccc345c99ffefed5e3572c5624b9'),
('55a54e57b0c70df220050f6dc1f020bf','747e0a229710f0de896f3828e087fe6c','2591ccc345c99ffefed5e3572c5624b9'),
('5628e4625ee70553b2940a2c4ec53acf','cf3e0cb36a32711f56e1a59eae089117','2591ccc345c99ffefed5e3572c5624b9'),
('577afcc29564841725c571a7959fc695','b44235812ab7499bdcdc97c30d040ba8','132df2301625dc37fd1b4c258d4a2838'),
('581d0bcceff34d41ffcaa2aa4ac10a0d','20373943e30630f2fc613a2b02cb39ff','2591ccc345c99ffefed5e3572c5624b9'),
('58282453d5f0c27da1ebf5ab2cf71140','dc2b9a1f31d6d4cc6828cd7c2eecdc24','2591ccc345c99ffefed5e3572c5624b9'),
('5bedc1609004e9ebff7ed634565560d1','df7e347824f011d19bbf8511da66b4ff','2591ccc345c99ffefed5e3572c5624b9'),
('5bf94f337b93c26145fe92fc2ed69c81','d854cbd43bfc338c3b361a1f2761b04f','2591ccc345c99ffefed5e3572c5624b9'),
('5de9e7e8c4670a82a03ab9f1705d8a0b','08b81fc54c02cd784cf6e0fb53532a58','2591ccc345c99ffefed5e3572c5624b9'),
('601bc2f2b5f0a70038c1f03020de87c4','a7387bff5209ba22bd8ff6af79bec3a1','2591ccc345c99ffefed5e3572c5624b9'),
('60a367fa2fa5fd2482fe866ee3a60b30','4db7ce6ad6a41bda81b0bea914001826','2591ccc345c99ffefed5e3572c5624b9'),
('60ac0f94556f61b38c9dd8b3a8be436c','2ff2be4eca8133c06109aab30984b7dd','2591ccc345c99ffefed5e3572c5624b9'),
('6119b7d1f30230e0678b83a60ba430ad','0bd86b301f1e981e2806777acec8c896','25584e184e69b7df3a842cad90e5dc55'),
('61f8faa41547b0918c4757c6816efcc1','0876f67d9d0ce57c274b72d67b0b8644','b5e6af275bd0cfacc09bf9af7550e3c7'),
('620c8d9f0f0e2d955e54a260e6e01a51','41b86bb946141ca9406b81fa47a978eb','2591ccc345c99ffefed5e3572c5624b9'),
('6279413c0d47f331b0ebdc53dae7f43d','b31def5ceaf1fab0dd8848c371e5132e','dadd65d82a694cc49288c930fd5527f5'),
('631717a8dc30779215afd5786c4eb93d','ab449381618e9a17b0292c600630da5c','132df2301625dc37fd1b4c258d4a2838'),
('68653b03232ce20b68e1fe83c544a455','ce6d938bde4b9ac904ae971682976936','dadd65d82a694cc49288c930fd5527f5'),
('69516fb8b024bb79fb2c4c0f5194b020','80750adc8251f609797cc532c3aa5421','2591ccc345c99ffefed5e3572c5624b9'),
('69d21c9ad333cb1b157346def69e2889','878536514e5ea46cbee965a3dfdfc704','2591ccc345c99ffefed5e3572c5624b9'),
('6c1eed030b70a328192bf661d477dc96','4c318706940095c0f1a6a680852679e7','2591ccc345c99ffefed5e3572c5624b9'),
('6e348abe22fc4f2df465a9747ed79cbb','dc2b9a1f31d6d4cc6828cd7c2eecdc24','132df2301625dc37fd1b4c258d4a2838'),
('6f1dce0ca1f84e08933c180252b16e7c','f63374206f110b90720eaedd349b07c6','2591ccc345c99ffefed5e3572c5624b9'),
('6fecb3e836d90910fc1eaf1c659e32bf','a040c4c9ee80e5f3b6b1548be8f8b3f4','2591ccc345c99ffefed5e3572c5624b9'),
('70cb20efc572ec40199b8bdb7a590279','e9605fee6c5c251215fb897f7eb8dd48','2591ccc345c99ffefed5e3572c5624b9'),
('711185d604f53ae223f2c355927ccdb5','8a9f73fa4aeb410284cf12925f31fa63','2591ccc345c99ffefed5e3572c5624b9'),
('7138477b116b57876b7d61a3988a9c51','ba9ea0fc67d525f0b8269527cef3eae8','2591ccc345c99ffefed5e3572c5624b9'),
('7566af9dbef150be1e617d3c258239df','878536514e5ea46cbee965a3dfdfc704','132df2301625dc37fd1b4c258d4a2838'),
('7587be3a97865177f3667cf549c8ca3c','2491ae6d4a046a60ab27c9ed33d26a89','2591ccc345c99ffefed5e3572c5624b9'),
('763f1828f997ef82dae0b3170233f301','0876f67d9d0ce57c274b72d67b0b8644','2591ccc345c99ffefed5e3572c5624b9'),
('78f812c2147952d2a7a3eb77b4d2f6cc','41b86bb946141ca9406b81fa47a978eb','25584e184e69b7df3a842cad90e5dc55'),
('7acfecd98f31c1d0e4a87ec9b41bbf3f','be1ac761243b1622c9c6071ed473ac73','132df2301625dc37fd1b4c258d4a2838'),
('7ca66e438ed1bd3165ce28a0315b86f9','24eb7b598b415e1962053e4640047bd8','25584e184e69b7df3a842cad90e5dc55'),
('7cc576ed78e46633bcad77661096d3f5','5b8395d85703d7f512e1eade489e8a3b','2591ccc345c99ffefed5e3572c5624b9'),
('7e37eb856679f7189314144ccec57d71','19e541e076d570093f2e00537b9c4b62','2591ccc345c99ffefed5e3572c5624b9'),
('7e97f1306697f4a0d23fa0c79843a128','a535d1b325bc33b6b69890a2fc0df471','2591ccc345c99ffefed5e3572c5624b9'),
('7f624d9086fa6ce779d98735e7582a87','9f524ee3d4900eb3c335a65b5966c205','2591ccc345c99ffefed5e3572c5624b9'),
('81f34eaeb87eaa55a5ce7e2f9d200674','dc624a9a5709940a9fabf06a6dc888d4','2591ccc345c99ffefed5e3572c5624b9'),
('82b7d626eeb3cf58dc4a3876e3fff13e','406bbfa459f24fa8f91ac4c6242125b2','132df2301625dc37fd1b4c258d4a2838'),
('83b52f5b8a8f8c35d4ab305b980110a1','d4695b2d99addc0bbc1de95d4d72a822','2591ccc345c99ffefed5e3572c5624b9'),
('83db2678fa3f3db1a973658d37182e99','f06d2505422ae5eeb2d4383bce1ec308','2591ccc345c99ffefed5e3572c5624b9'),
('84379e3151faf6bca11f0e988ebdca4e','0c897c7a830230131fdf1294952d268c','2591ccc345c99ffefed5e3572c5624b9'),
('8486a132f2b59be9bf2aa116b8c92565','ab449381618e9a17b0292c600630da5c','2591ccc345c99ffefed5e3572c5624b9'),
('84c96cbee5efb7c0b7fb123d4e3314b4','5768adeb7423791a5d7b4a7c01e8606d','132df2301625dc37fd1b4c258d4a2838'),
('84ee6e0af9864f350e86235c7ca5d871','b6c3802052a36658215ac1bc95158639','2591ccc345c99ffefed5e3572c5624b9'),
('85287536bda7b5b13696353dfa44c631','bcc5ec4b4b74944a31f91db81c1c475e','dadd65d82a694cc49288c930fd5527f5'),
('853c5f3b44535a323c1537a91c89ac44','586cd5672e2790b5c05afeeda8fb295e','2591ccc345c99ffefed5e3572c5624b9'),
('8637a6305bde378bdc4121ff94fde65f','08f70e990d1826febe7c98b08316fd84','132df2301625dc37fd1b4c258d4a2838'),
('867d7ab59753add08a43eeea91e066e2','58c33112974e8a4ccb7b77d01e0e6375','dadd65d82a694cc49288c930fd5527f5'),
('86e78aebd1936ce4cba1b6d9c84d9c04','e48a7340fbe6e83063160dd02a51dd9c','132df2301625dc37fd1b4c258d4a2838'),
('871c958d8e27a5ba264a7ab45c0a1536','e14872a2d9c5865429eab89a512fbd44','dadd65d82a694cc49288c930fd5527f5'),
('8777847f744a5ff328f642b11ff9d6f6','f82e6f555c560ecf48d00a1761446846','2591ccc345c99ffefed5e3572c5624b9'),
('88b6f450ea5dd874d1ecdb41af86e0b3','24eb7b598b415e1962053e4640047bd8','2591ccc345c99ffefed5e3572c5624b9'),
('890d190e3eb920b433b94cfbbb50fbd2','a898cc3deacc37750296ae5882206546','2591ccc345c99ffefed5e3572c5624b9'),
('8910d0ffec137d99e59cded09a17812e','6e58d06164f8e464d64a272e2fd17b40','2591ccc345c99ffefed5e3572c5624b9'),
('89bd92af2ef9686360098824fac7e753','c0794857aca9156c0f0ba4d6a738d3fb','2591ccc345c99ffefed5e3572c5624b9'),
('8b9299881b41c16c627b30ba468d02b7','8a5273ec71b323883ce08178a198e872','2591ccc345c99ffefed5e3572c5624b9'),
('8f0e57281225f1fa835f12b102e45f30','334c8e3d31d084817ead4e11091ef706','25584e184e69b7df3a842cad90e5dc55'),
('8fef855790f1c1f3598bdefdb12e8c1d','63931ada9418b9a0d6e16c5e9240a60b','25584e184e69b7df3a842cad90e5dc55'),
('914c5e53805a214282d543979540e62f','8b1d9c7579e86e94a2d3eb8753b1d91a','132df2301625dc37fd1b4c258d4a2838'),
('9169dfbf9169c408ba83103a56a55fec','5921a673805bcf994b6cec5b1bc4faf7','2591ccc345c99ffefed5e3572c5624b9'),
('94e5051532eab81e5dd5f182191bae62','4e6380bed6b288a95678ba9d587febd5','132df2301625dc37fd1b4c258d4a2838'),
('958858b35af8ca39a5d3a578f1e67ac8','1389cc79b92ceddfe21b6fbed368a531','dadd65d82a694cc49288c930fd5527f5'),
('979d671e209f21e58ed6a473220dd198','8b1d9c7579e86e94a2d3eb8753b1d91a','2591ccc345c99ffefed5e3572c5624b9'),
('97d7d9b06568a627e83358ca09cf7e1f','9dfcd764089657442ab4e9db84180cf0','25584e184e69b7df3a842cad90e5dc55'),
('98ea3677f21c49a2f03366a0bbde3d18','8271911c11770eaf05e8e0705d65a3b4','132df2301625dc37fd1b4c258d4a2838'),
('9aabf83a95510162a60ff56c85981aab','ceb2c856774667386df2e5068dc0962f','2591ccc345c99ffefed5e3572c5624b9'),
('9b3ae6856d89d95ce13beb1828d46f4b','d11ace81698866e3f35ce9e316c28b1d','2591ccc345c99ffefed5e3572c5624b9'),
('9b7431d49218311b965c44d3e253f481','3c8e54f95b9967fdbdf6fc2a516e4057','2591ccc345c99ffefed5e3572c5624b9'),
('9c2e04866bb3301206d2167ee4809f15','a5b91e0416600298e32fa9ea4f39639a','dadd65d82a694cc49288c930fd5527f5'),
('9f6cb3c70828a724c65eb4fbde921c21','d11ace81698866e3f35ce9e316c28b1d','dadd65d82a694cc49288c930fd5527f5'),
('a09ed94a033c0f23a17ce3a9db15fe0e','8fe226c555d300dc27b96258589de9b6','132df2301625dc37fd1b4c258d4a2838'),
('a2bc623ac8bde923bf2d7ab48c555a7e','98803ee7e10ccb7a06400e33b37ec7df','2591ccc345c99ffefed5e3572c5624b9'),
('a2dbfff92990d2a6008bef552d1c4b6f','2fee25e9d7326b990846de269289e162','2591ccc345c99ffefed5e3572c5624b9'),
('a407fd4a8f38f9f08eaa9ed2da6918c6','ba9ea0fc67d525f0b8269527cef3eae8','b5e6af275bd0cfacc09bf9af7550e3c7'),
('a8a051e8c4d03d8b6976e63c3150a1c1','7cda98134d6d6b7b5dcb86eb6caeed92','132df2301625dc37fd1b4c258d4a2838'),
('a8cb810917d8d18ab680fa9ec8c0f779','5c754dd3c682d33cd5507c1e6415de95','2591ccc345c99ffefed5e3572c5624b9'),
('a9e13a3d07a343969859c1657094261c','51e36593a943a0f25bcadbba630f376f','132df2301625dc37fd1b4c258d4a2838'),
('aad57c414fe93c3b71b96fa2919705c2','c27a00a04329ec0ac97bcea7ebec1ec7','2591ccc345c99ffefed5e3572c5624b9'),
('aaf322f39f88f63d7370a9f6ca9c1055','5921a673805bcf994b6cec5b1bc4faf7','132df2301625dc37fd1b4c258d4a2838'),
('ac2fab050ca897e81c1e7e4068b4b29a','2134076d72b4b7d88b4a878c4065b60f','dadd65d82a694cc49288c930fd5527f5'),
('ace6653e8666274696e104321e2e1b08','91788c7ce2a2d074765d12bc95b6d287','2591ccc345c99ffefed5e3572c5624b9'),
('ad4c69e70b0045343509f1ebbcc67a17','9f524ee3d4900eb3c335a65b5966c205','132df2301625dc37fd1b4c258d4a2838'),
('ae2c21d3510a0d97f9936442a79ffebf','51e36593a943a0f25bcadbba630f376f','2591ccc345c99ffefed5e3572c5624b9'),
('b0e8acf85ba26782167814256e0649d6','15ff00cd8cfb9f0334402b3792cc2744','dadd65d82a694cc49288c930fd5527f5'),
('b355f8a755bc7ebe6eb1e33d61c5b48b','8a9f73fa4aeb410284cf12925f31fa63','132df2301625dc37fd1b4c258d4a2838'),
('b5c2616ff9e94d948a67e2cc8242ccb4','4e6380bed6b288a95678ba9d587febd5','2591ccc345c99ffefed5e3572c5624b9'),
('b65b10f55143e5bc949fde6c17ccb78a','87d3f5f7bf12dfc795c2e99bcae3cda0','2591ccc345c99ffefed5e3572c5624b9'),
('b707d40595b63f470e0f11ac1e43ed7f','6b1afce9c445e0e96a03f902d2a05ea3','132df2301625dc37fd1b4c258d4a2838'),
('b81670b52f1912858a46df52c396d697','b3c777ecec0a944227b7d8f8261b51d1','2591ccc345c99ffefed5e3572c5624b9'),
('b8aabb63b21822e1b622806e80ed325c','c27a00a04329ec0ac97bcea7ebec1ec7','132df2301625dc37fd1b4c258d4a2838'),
('b9f06b79042bd8bf7e5ee72d5048319e','4741eca6b5586f35f0b051ff9d7c11cc','2591ccc345c99ffefed5e3572c5624b9'),
('baa5571e98756c63cfbfc85cd6cf2778','7cda98134d6d6b7b5dcb86eb6caeed92','2591ccc345c99ffefed5e3572c5624b9'),
('bbbd5b7881af77160f28f82ebf38e08e','05332425de2d63066c482b5d3bb1e619','132df2301625dc37fd1b4c258d4a2838'),
('bbdd3be03d1daae11eb687827dca8df2','b78d9aad0da7760019a851c367de8298','2591ccc345c99ffefed5e3572c5624b9'),
('bd8f0f6c7dce791e6febcf6ce2f77d12','4252497f4fceeaebef3ec62a29e21d80','2591ccc345c99ffefed5e3572c5624b9'),
('c1787455433a1b891fcc78f739fcd570','cf3e0cb36a32711f56e1a59eae089117','b5e6af275bd0cfacc09bf9af7550e3c7'),
('c1f1d8fb94d62e7aa61f14a09c75ddd4','8a2429c86f2258a88d0b94354cab6c0f','132df2301625dc37fd1b4c258d4a2838'),
('c2410112bb3878cd6b602450aec89303','9b5ba1d0d67a05b5ab7d2634081bfa6d','2591ccc345c99ffefed5e3572c5624b9'),
('c264aea775e0db53ac860afb0e701569','80750adc8251f609797cc532c3aa5421','dadd65d82a694cc49288c930fd5527f5'),
('c2ddd697ae40585be7421895bef97de8','22ea663c6f25cb1c70317c0e11affb9a','132df2301625dc37fd1b4c258d4a2838'),
('c3a5a50a8811161268ea80dc20a4c02b','cdb357370d5b361a008c557f985e44d4','dadd65d82a694cc49288c930fd5527f5'),
('c4e16179d740958db0fb5aaf54a5b65b','08f70e990d1826febe7c98b08316fd84','2591ccc345c99ffefed5e3572c5624b9'),
('c5cfefaeb9e070041aee0d33e6ce59fd','7e3bf80e3941afe908be70ba8b9603f8','132df2301625dc37fd1b4c258d4a2838'),
('c692c3db62ceb1fe8337ebc23a6ceac8','b30d86c9c0899857fca2d91b29edae2a','132df2301625dc37fd1b4c258d4a2838'),
('cadd7fbb776bbdee239f40d16c032229','dea9872e3c1a186f2ff9bbfb63351a6f','dadd65d82a694cc49288c930fd5527f5'),
('cb6a182d147563a0723bb7c32ea49d99','35d48b39edbe08e6a657674a8e2e901f','dadd65d82a694cc49288c930fd5527f5'),
('cd063b64a8b06607a338d4646f67597f','e31f3ec92a643dc6bc1da728417aa9f9','132df2301625dc37fd1b4c258d4a2838'),
('cd0d36650003e0b41389119952ad7deb','059358cc4487e7773a878cda0193de62','dadd65d82a694cc49288c930fd5527f5'),
('cd3862a6f3f937b87dfe3f94b121a55e','98803ee7e10ccb7a06400e33b37ec7df','dadd65d82a694cc49288c930fd5527f5'),
('cd6c5e7f85d1091ab151461d090c166c','406bbfa459f24fa8f91ac4c6242125b2','2591ccc345c99ffefed5e3572c5624b9'),
('cd88a1c499538e51ec004904850b228c','b1c31fb1b64259b8ffb83a31bc445475','132df2301625dc37fd1b4c258d4a2838'),
('cf1e87fe365729aa7c2686668a6dcc70','92ab7f66e76815d7fa0c34927a189a94','2591ccc345c99ffefed5e3572c5624b9'),
('d020a59bd5341dd17534e00b8acaf131','1f151581825c40bc09b9f65064509824','2591ccc345c99ffefed5e3572c5624b9'),
('d1b940980e60f3ada4686f684231a78b','4903fa7d0ce151223015f4e3ee5890bf','2591ccc345c99ffefed5e3572c5624b9'),
('d3c22ee0b354c527844cfe1eb323faf0','b44235812ab7499bdcdc97c30d040ba8','2591ccc345c99ffefed5e3572c5624b9'),
('d41a112e2857d95b8c480ddde7c19f2e','d64f53e1ddd625f7ce895c7db62c09c3','2591ccc345c99ffefed5e3572c5624b9'),
('d5ed7717f25a1abbc9f6d2f71ff6d42d','22ea663c6f25cb1c70317c0e11affb9a','2591ccc345c99ffefed5e3572c5624b9'),
('d5fd40412189831fd246614d78d38775','a4451d6e3cbfe5051f8f7b2e24caa2df','2591ccc345c99ffefed5e3572c5624b9'),
('d629adf781646675a8f86b1e0a8ae186','a4451d6e3cbfe5051f8f7b2e24caa2df','132df2301625dc37fd1b4c258d4a2838'),
('d6a36798137248e792221b47777cef53','5171d0aaa101d9052c04719f1670e09b','2591ccc345c99ffefed5e3572c5624b9'),
('d722f0dec23a800de5acff54610ae64d','e48a7340fbe6e83063160dd02a51dd9c','2591ccc345c99ffefed5e3572c5624b9'),
('d72755272c82d5d9193ce66223056042','7255875725e897036ee1eafc63efa67c','132df2301625dc37fd1b4c258d4a2838'),
('d7bf6473b5fa95e2a2a7914dad96303b','45cccf603e4610967a7dbf9f927a63c6','2591ccc345c99ffefed5e3572c5624b9'),
('d8087252a650804626d246892b3cddd7','46969de817040b955c8fb3911b8691b6','2591ccc345c99ffefed5e3572c5624b9'),
('d81b51051f989ea9c733e496c6eaab62','5e08460d5a897ca506c0b80cb6fc6d79','2591ccc345c99ffefed5e3572c5624b9'),
('d93266768893528ee4e7edb6bc6747ef','8c23b7714200c43cfeef247b9e745058','2591ccc345c99ffefed5e3572c5624b9'),
('dbc17ff34ed5261c235043691d67abd8','a293d96f22bf46ed649fa12ac38594d8','2591ccc345c99ffefed5e3572c5624b9'),
('dd731448893ab33ca7cd06dacafe98e0','a7387bff5209ba22bd8ff6af79bec3a1','132df2301625dc37fd1b4c258d4a2838'),
('e01d7947d99030eea2b1234d32413162','d23726402e41042149ac9467c8de43e1','dadd65d82a694cc49288c930fd5527f5'),
('e03887a18ef7c7e36cb67d0dccffd1fb','8fd87ffcd7852e8666fd186f888c18cd','2591ccc345c99ffefed5e3572c5624b9'),
('e2d080d22d7052b55de4776f086b9fca','8e8bee9cbc214fe4e09458c78f211212','2591ccc345c99ffefed5e3572c5624b9'),
('e2ff486b7a9bcea98dbab516d9c2c02b','92501d43694f22d03f5e503b6abe26d8','2591ccc345c99ffefed5e3572c5624b9'),
('e56855bac01af949b2227ec2ef9e03a0','0bd86b301f1e981e2806777acec8c896','2591ccc345c99ffefed5e3572c5624b9'),
('e5e82730873a2052ee693a1da6211a04','e9605fee6c5c251215fb897f7eb8dd48','132df2301625dc37fd1b4c258d4a2838'),
('e69b9031f063d1a1266391b47249f745','83bb91e20c261cbe9c3f86dfd2b08ef5','2591ccc345c99ffefed5e3572c5624b9'),
('e6b2d85db6876391ed6623a0736b3c12','a59637687c7f070251cb7ade49f5c643','2591ccc345c99ffefed5e3572c5624b9'),
('e82d886c8762a0e00b70a6582667d1c2','9dfcd764089657442ab4e9db84180cf0','2591ccc345c99ffefed5e3572c5624b9'),
('e897be48d09bf42860b35bdb59e60156','5768adeb7423791a5d7b4a7c01e8606d','2591ccc345c99ffefed5e3572c5624b9'),
('eafc19e25397f599516eed93c6e85aa4','82156e74423c47004a8974f3a6b15760','132df2301625dc37fd1b4c258d4a2838'),
('eb9e6aafb6c819997f085efe3cfcfc96','a898cc3deacc37750296ae5882206546','132df2301625dc37fd1b4c258d4a2838'),
('ec016e92fad6416f2287bfb23bac148a','d0f33023c9a0848e65db5606c336e3ca','dadd65d82a694cc49288c930fd5527f5'),
('ef3c6fac1b5d15b2d81891bd10f508c8','b8915891e54932df280785df69ab1b5a','dadd65d82a694cc49288c930fd5527f5'),
('f178eff929e7ff41aa53d7454661d398','9912fad6f3e3f59248f4a5383a7daf1a','2591ccc345c99ffefed5e3572c5624b9'),
('f1e6c117a32d8124520ffcfb6ae3dac9','d589cb313d9ca471bfc8310a7405cc44','dadd65d82a694cc49288c930fd5527f5'),
('f24e5e6593ef6b1ef02c99ae73164cc4','dfd5818e4d4d8ddd2491adb39ab6a3a5','2591ccc345c99ffefed5e3572c5624b9'),
('f39c4ccc48146151bba5b5d9bb3363b9','b30d86c9c0899857fca2d91b29edae2a','2591ccc345c99ffefed5e3572c5624b9'),
('f44d3a1e070396341ba2a5ce3cc145d7','c943d2ef68d8b3f6d524d8e828827b8a','2591ccc345c99ffefed5e3572c5624b9'),
('f50b9c14bc5016ff9f773643604e6caa','74ab62cfb0c0e1ab742fa8ec50e4ea68','2591ccc345c99ffefed5e3572c5624b9'),
('f6243e4810724b0c693569d1689b906a','ce6d938bde4b9ac904ae971682976937','dadd65d82a694cc49288c930fd5527f5'),
('f7535c2fc60de0dfe0af332e883564cb','8a5273ec71b323883ce08178a198e872','132df2301625dc37fd1b4c258d4a2838'),
('f770646c01c6d9c5075e357b9f238121','f63374206f110b90720eaedd349b07c6','132df2301625dc37fd1b4c258d4a2838'),
('f8eacd98b3dd84a96d2b1e872f1d3eeb','7f7bcb8e1ae8bb610e726bc905b1c2a1','2591ccc345c99ffefed5e3572c5624b9'),
('fa8fb8a5a2d4a758a4fef63d67172be4','8fe226c555d300dc27b96258589de9b6','2591ccc345c99ffefed5e3572c5624b9'),
('fc67ca5658aaebe60f415471523917bf','ac5d77d0cb87c8cbe2cf7881a5ae75e7','2591ccc345c99ffefed5e3572c5624b9'),
('fcb777ca0a826568bb284c47a3bab997','643986b77c9a659f9f918e2de045aab0','132df2301625dc37fd1b4c258d4a2838'),
('fccfaed1006df93832a7f8218b051054','74ab62cfb0c0e1ab742fa8ec50e4ea68','132df2301625dc37fd1b4c258d4a2838'),
('fce2aa830f81a6c9d8a73972325bc1c4','5768adeb7423791a5d7b4a7c01e8606d','b5e6af275bd0cfacc09bf9af7550e3c7'),
('fe50f868556eb65312ed554b1b5354d6','762f77999c73b7b249714cbdd1f52694','2591ccc345c99ffefed5e3572c5624b9'),
('ff219fc39aafd547e66ed98f5fe5c332','35ba3a88e51b9c0a0da89afafaa04e28','132df2301625dc37fd1b4c258d4a2838'),
('ff9e7841114778445968704d8e71aa39','7273b375695b6301e362ac260b7101ef','132df2301625dc37fd1b4c258d4a2838'),
('ffec1e6c3ef9bf8a270fe11eea9da731','e31f3ec92a643dc6bc1da728417aa9f9','2591ccc345c99ffefed5e3572c5624b9');

/*Table structure for table `t_sys_role_info` */

DROP TABLE IF EXISTS `t_sys_role_info`;

CREATE TABLE `t_sys_role_info` (
  `role_id` varchar(32) NOT NULL COMMENT '角色号',
  `code` varchar(255) DEFAULT NULL COMMENT '角色编码',
  `title` varchar(255) DEFAULT NULL COMMENT '角色标题',
  `detail` varchar(255) DEFAULT NULL COMMENT '角色备注',
  `status` int(1) DEFAULT NULL COMMENT '状态：1：启用、0：禁用、-1：删除',
  PRIMARY KEY (`role_id`) USING BTREE,
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户角色表';

/*Data for the table `t_sys_role_info` */

insert  into `t_sys_role_info`(`role_id`,`code`,`title`,`detail`,`status`) values 
('c00e88466147697e63781dfdd1cb8efd','app','App用户','',1),
('dadd65d82a694cc49288c930fd5527f5','dev','开发者','',1);

/*Table structure for table `t_sys_user_basic_info` */

DROP TABLE IF EXISTS `t_sys_user_basic_info`;

CREATE TABLE `t_sys_user_basic_info` (
  `sys_user_id` varchar(32) NOT NULL,
  `code` varchar(32) DEFAULT NULL COMMENT '用户编码',
  `nice` varchar(50) DEFAULT NULL COMMENT '昵称',
  `face` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` int(1) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `province_id` varchar(32) DEFAULT NULL COMMENT '省编号',
  `city_id` varchar(32) DEFAULT NULL COMMENT '市编号',
  `area_id` varchar(32) DEFAULT NULL COMMENT '区县编号',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱地址',
  `type` int(1) DEFAULT NULL COMMENT '用户类型，0：后台、1：小程序',
  `qq` varchar(255) DEFAULT NULL,
  `weixin` varchar(255) DEFAULT NULL,
  `sign` varchar(1) DEFAULT NULL COMMENT '雷达标记',
  `online` int(1) DEFAULT NULL COMMENT '在线状态',
  `balance` double(8,4) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL COMMENT '设备指纹',
  `position_id` varchar(32) DEFAULT NULL COMMENT '职位号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_date` int(8) DEFAULT NULL COMMENT '创建日期',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `last_update_date` int(8) DEFAULT NULL COMMENT '最后更新日期',
  PRIMARY KEY (`sys_user_id`) USING BTREE,
  KEY `code` (`code`),
  KEY `type` (`type`),
  KEY `sign` (`sign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户基本信息表';

/*Data for the table `t_sys_user_basic_info` */

insert  into `t_sys_user_basic_info`(`sys_user_id`,`code`,`nice`,`face`,`sex`,`birthday`,`phone`,`province_id`,`city_id`,`area_id`,`address`,`email`,`type`,`qq`,`weixin`,`sign`,`online`,`balance`,`token`,`position_id`,`create_time`,`create_date`,`last_update_time`,`last_update_date`) values 
('0ddd5d1dda5b1e433c09eb5115359355',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-07-15 10:07:26',20230715,'2023-07-15 10:07:26',20230715),
('15d43da716e3cdb2c963b538f280d09a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023-07-16 16:15:05',20230716,'2023-07-16 16:15:05',20230716),
('1c0fe90b8d62478c9eb8eb3b02617725','0001','管理员',NULL,1,NULL,'15200000000',NULL,NULL,NULL,'无锡','1420527913@qq.com',0,'1420527913','1420527913',NULL,NULL,NULL,NULL,NULL,'2023-07-13 11:18:46',20230713,'2023-07-13 21:26:09',20230713),
('6ed8c79e642d71e22c2553cc3c8d8ae0',NULL,'用户1','1689398395806.jpg',1,'2023-07-13','15211111111',NULL,NULL,NULL,'南京雨花台','user1@qq.com',0,'549710689','bishe2022',NULL,NULL,NULL,NULL,NULL,'2023-07-13 11:18:46',20230713,'2023-07-15 13:30:14',20230715),
('77ac876188a74743deddf78383a239aa',NULL,'用户3','1689400741425.jpg',NULL,NULL,'15233333333',NULL,NULL,NULL,'333','user3@qq.com',0,'3333','3333',NULL,NULL,NULL,NULL,NULL,'2023-07-15 13:59:02',20230715,'2023-07-15 13:59:45',20230715);

/*Table structure for table `t_sys_user_info` */

DROP TABLE IF EXISTS `t_sys_user_info`;

CREATE TABLE `t_sys_user_info` (
  `sys_user_id` varchar(32) NOT NULL,
  `account` varchar(30) NOT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `roles` varchar(32) DEFAULT NULL COMMENT '用户角色列表',
  `status` int(2) DEFAULT NULL COMMENT '状态：1：启用、0：禁用、-1：删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_date` int(8) DEFAULT NULL COMMENT '创建日期',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `last_update_date` int(8) DEFAULT NULL COMMENT '最后更新日期',
  PRIMARY KEY (`sys_user_id`) USING BTREE,
  KEY `index_sys_user_account` (`account`) USING BTREE,
  KEY `index_sys_user_password` (`password`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户账号表';

/*Data for the table `t_sys_user_info` */

insert  into `t_sys_user_info`(`sys_user_id`,`account`,`password`,`roles`,`status`,`create_time`,`create_date`,`last_update_time`,`last_update_date`) values 
('0ddd5d1dda5b1e433c09eb5115359355','user2','e10adc3949ba59abbe56e057f20f883e','',1,'2023-07-15 10:07:26',20230715,'2023-07-15 10:07:26',20230715),
('15d43da716e3cdb2c963b538f280d09a','user5','e10adc3949ba59abbe56e057f20f883e','',1,'2023-07-16 16:15:05',20230716,'2023-07-16 16:15:05',20230716),
('1c0fe90b8d62478c9eb8eb3b02617725','admin','e10adc3949ba59abbe56e057f20f883e','dadd65d82a694cc49288c930fd5527f5',1,'2023-07-13 15:57:18',20230713,'2023-07-13 21:26:09',20230713),
('6ed8c79e642d71e22c2553cc3c8d8ae0','user1','e10adc3949ba59abbe56e057f20f883e','c00e88466147697e63781dfdd1cb8efd',1,'2023-07-13 11:18:46',20230713,'2023-07-15 13:30:14',20230715),
('77ac876188a74743deddf78383a239aa','user3','e10adc3949ba59abbe56e057f20f883e','dadd65d82a694cc49288c930fd5527f5',1,'2023-07-15 13:59:02',20230715,'2023-07-15 13:59:45',20230715);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
