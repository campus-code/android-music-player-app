# Android音乐播放器app

#### 介绍
Android音乐播放器app(IDEA,SpringBoot,SSM,MySQL)+全套视频教程

【项目功能介绍】

    本系统包含后台管理和前端app双端系统，后台管理的功能包含: 登录, 退出, 修改管理员信息(基本信息与头像),资源管理,角色管理,资源权限分配,字典管理,用户管理,音乐管理;  app端功能:注册，登录，轮播图,网络音乐,收藏点赞音乐,播放音乐,搜索音乐,本地音乐,播放本地音乐,用户基本信息管理,用户头像修改,密码修改,我的收藏,用户退出

【技术栈】

    Web端                               

    后台前端：layui,js、jQuery、css、html       

    后台框架：Java、Spring boot、Spring Mvc、Mybatis Plus、Shiro、ajax

    数据库：Mysql5.7

    App客户端

    前端框架：xml、LinearLayout、RelativeLayout

    后台框架：OkHttp、fastJson   

![输入图片说明](image/1image.png)
![输入图片说明](image/2image.png)
![输入图片说明](image/3image.png)

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
